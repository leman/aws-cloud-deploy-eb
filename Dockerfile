ARG  BASE_VERSION=latest
FROM registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:${BASE_VERSION}

# Install jq package
RUN apt-get update -y && apt-get install -y software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa -y

# Install python
RUN apt-get install -y python3.7

# Download pip
RUN apt-get install -y python3-pip

ENV PATH="/root/.local/bin:${PATH}"

RUN echo $PATH

RUN pip3 install awsebcli --upgrade --user

RUN python3.7 --version

RUN eb --version

RUN apt-get install gettext-base

RUN envsubst --version

